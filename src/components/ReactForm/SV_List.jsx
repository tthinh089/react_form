import React, { Component } from "react";

export default class SV_List extends Component {
  render() {
    const { data, handleDelete, handleStartEdit } = this.props;
    return (
      <div className="mt-4">
        <h3>Danh sách sinh viên</h3>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Mã SV</th>
              <th scope="col">Họ Tên</th>
              <th scope="col">Số Điện Thoại</th>
              <th scope="col">Email</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => {
              return (
                <tr key={item.id}>
                  <th>{item.id}</th>
                  <td>{item.name}</td>
                  <td>{item.phoneNumber}</td>
                  <td>{item.email}</td>
                  <td>
                    <button
                      className="btn btn-warning"
                      onClick={() => {
                        handleStartEdit(item.id);
                      }}
                    >
                      Edit
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        handleDelete(item.id);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
