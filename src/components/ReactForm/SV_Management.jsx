import React, { Component } from "react";
import SV_List from "./SV_List";

export default class SV_Management extends Component {
  state = {
    values: {
      id: "",
      name: "",
      phoneNumber: "",
      email: "",
    },
    errors: {
      id: "",
      name: "",
      phoneNumber: "",
      email: "",
    },
    listSV: [
      {
        id: "123ABC",
        name: "Nguyen Van A",
        phoneNumber: "09314156517",
        email: "@gmail.com",
      },
    ],
    editingSV: null,
  };

  formValid = () => {
    let isValid = false;
    Object.values(this.state.values).forEach((value) => {
      if (!value) {
        isValid = true;
      }
    });
    return isValid;
  };

  handleOnChange = (e) => {
    const { name, value } = e.target;

    const newValue = { ...this.state.values, [name]: value };
    const newError = { ...this.state.errors };
    if (!value.trim()) {
      newError[name] = "This field is required!";
    } else {
      newError[name] = "";
      const pattern = e.target.getAttribute("pattern");
      if (pattern) {
        const regex = new RegExp(pattern);
        const invalid = !regex.test(value);
        if (invalid) {
          newError[name] = `${name} is invalid`;
        }
      }
    }

    this.setState({ values: newValue, errors: newError });
  };

  handleOnBlur = (e) => {
    const { name, value } = e.target;

    const newValue = { ...this.state.values, [name]: value };
    const newError = { ...this.state.errors };
    if (!value.trim()) {
      newError[name] = "This field is required!";
    } else {
      newError[name] = "";
    }
    this.setState({ values: newValue, errors: newError });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (!this.formValid()) {
      const newProducts = [...this.state.listSV, this.state.values];
      this.setState({ listSV: newProducts });
      console.log(this.state.values);
    }
  };

  handleDelete = (maSV) => {
    console.log(maSV);
    let locate = this.state.listSV.findIndex((item) => {
      return item.id === maSV;
    });
    if (locate !== -1) {
      const newListSV = [...this.state.listSV];
      newListSV.splice(locate, 1);
      this.setState({ listSV: newListSV });
    }
  };

  handleStartEdit = (maSV) => {
    const locateSV = this.state.listSV.find((item) => {
      return item.id === maSV;
    });
    this.setState({ editingSV: locateSV, values: locateSV });
  };

  handleEdit = () => {
    const locateSV = this.state.listSV.findIndex((item) => {
      return item.id === this.state.editingSV.id;
    });
    const newListSV = [...this.state.listSV];
    newListSV[locateSV] = this.state.values;
    this.setState({
      listSV: newListSV,
      values: {
        id: "",
        name: "",
        phoneNumber: "",
        email: "",
      },
      editingSV: null,
    });
  };

  render() {
    let { values, errors, listSV, editingSV } = this.state;

    let { id, name, phoneNumber, email } = values;
    return (
      <div className="container">
        <h3>Thông tin sinh viên </h3>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-6 mb-3">
              <label htmlFor="">Mã SV</label>
              <input
                value={id}
                type="text"
                id="id"
                name="id"
                className="form-control"
                pattern=""
                onChange={this.handleOnChange}
                onBlur={this.handleOnBlur}
                disabled={editingSV}
              />
              {errors.id && (
                <span className="text text-danger">{errors.id}</span>
              )}
            </div>
            <div className="col-6 mb-3">
              <label htmlFor="">Họ Tên</label>
              <input
                value={name}
                type="text"
                id="name"
                name="name"
                className="form-control"
                pattern="^[a-zA-Z_ ]*$"
                onChange={this.handleOnChange}
                onBlur={this.handleOnBlur}
              />
              {errors.name && (
                <span className="text text-danger">{errors.name}</span>
              )}
            </div>
            <div className="col-6 mb-3">
              <label htmlFor="">Số điện thoại</label>
              <input
                value={phoneNumber}
                type="text"
                id="phoneNumber"
                name="phoneNumber"
                className="form-control"
                pattern="^[0-9]+$"
                onChange={this.handleOnChange}
                onBlur={this.handleOnBlur}
              />
              {errors.phoneNumber && (
                <span className="text text-danger">{errors.phoneNumber}</span>
              )}
            </div>
            <div className="col-6 mb-3">
              <label htmlFor="">Email</label>
              <input
                value={email}
                type="text"
                id="email"
                name="email"
                className="form-control"
                pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"
                onChange={this.handleOnChange}
                onBlur={this.handleOnBlur}
              />
              {errors.email && (
                <span className="text text-danger">{errors.email}</span>
              )}
            </div>
            <div className="col-12">
              {editingSV ? (
                <button
                  className="btn btn-success"
                  type="button"
                  onClick={this.handleEdit}
                >
                  Confirm Edit
                </button>
              ) : (
                <button
                  className="btn btn-primary m-2"
                  onClick={this.handleSubmit}
                  onBlur={this.handleOnBlur}
                >
                  Add
                </button>
              )}
            </div>
          </div>
        </form>
        <SV_List
          data={listSV}
          handleDelete={this.handleDelete}
          handleStartEdit={this.handleStartEdit}
        />
      </div>
    );
  }
}
