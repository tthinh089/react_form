import { useState } from "react";

import ReactForm from "./components/ReactForm/SV_Management";

function App() {
  return (
    <>
      <ReactForm />
    </>
  );
}

export default App;
